const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

const db = admin.firestore();

exports.registerAssistance = functions.firestore
  .document('assistance/{assistanceDate}/assistants/{studentCode}').onCreate((change, context) => {
    // Select student document and add 1 to the 'totalAssistance' value
    return db.collection('students').doc(`${context.params.studentCode}`).update({
      totalAssistance: admin.firestore.FieldValue.increment(1),
    });
  });
