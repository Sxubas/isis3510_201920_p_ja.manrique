package com.example.myapplication

import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import java.net.URL
import android.os.StrictMode


/**
 * Apologies in advance for a ton of 'machetes' in this app, I'm just getting started with kotlin
 */
class MainActivity : AppCompatActivity() {
    var DB_URL = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Set thread to allow block blocking calls (URL.readText)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        val bluetoothAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (bluetoothAdapter == null) {
            _print("El dispositivo no tiene bluetooth :(")
        }

        if (!bluetoothAdapter.isEnabled) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, 1 /* maxete máximo */)
        }

        var discoveryButton = findViewById<Button>(R.id.button)

        discoveryButton.setOnClickListener {
            var result = bluetoothAdapter.startDiscovery()
            if (result) {
                _print("Todo bientos, descubriendo...")
            } else {
                _print("RIP ):")
            }
        }

        var checkDbButton = findViewById<Button>(R.id.button2)

        checkDbButton.setOnClickListener {
            _print("Se mandó la wea")
            val jsonString = URL(DB_URL).readText().toLowerCase()

            if (jsonString.contains("201615449")) {
                _print("La asistencia tiene el código!")
            }
            else if (jsonString.contains("julián manrique")) {
                _print("La asistencia tiene el nombre completo!")
            }
            else if (jsonString.contains("manrique")) {
                _print("La asistencia tiene el apellido!")
            }
            else {
                _print("La db no registra ni el código, ni el nombre completo ni el apellido")
            }
        }
    }

    fun _print(text: String) {
        Toast.makeText(this.baseContext, text, Toast.LENGTH_SHORT).show()
    }
}

